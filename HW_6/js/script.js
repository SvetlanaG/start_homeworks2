// Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

const arr1 = ['hello', 'world', 23, '23', null]
arr1[5] = 'string'

const arrNew = arr1.filter((item) => item === 1)
console.log(arrNew)
const deleted = (arr) => {
  const falsyValues = ['hello', 'world', '23', 'string']
  return arr.filter((value) => !falsyValues.includes(value))
}
console.log(deleted(arr1))
