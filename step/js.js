var tab
var tabContent
window.onload = function () {
  tabContent = document.getElementsByClassName('tabContent')
  tab = document.getElementsByClassName('tab')
  hideTabsContent(1)
}
function hideTabsContent(a) {
  for (var i = a; i < tabContent.length; i++) {
    tabContent[i].classList.remove('show')
    tabContent[i].classList.add('hide')
    tab[i].classList.remove('activeborder')
  }
}
document.getElementById('tabs').onclick = function (event) {
  var target = event.target
  if (target.className == 'tab') {
    for (var i = 0; i < tab.length; i++) {
      if (target == tab[i]) {
        showTabsContent(i)
        break
      }
    }
  }
}
function showTabsContent(b) {
  if (tabContent[b].classList.contains('hide')) {
    hideTabsContent(0)
    tab[b].classList.add('activeborder')
    tabContent[b].classList.remove('hide')
    tabContent[b].classList.add('show')
  }
}

const workMenu = document.querySelector('.works-menu')
items = document.querySelectorAll('.works-gallery-item')

function filter() {
  workMenu.addEventListener('click', (event) => {
    const targetId = event.target.dataset.id
    console.log(targetId)

    switch (targetId) {
      case 'all':
        getItems('works-gallery-item')
        break
      case 'design':
        getItems(targetId)
        break

      case 'webdesign':
        getItems(targetId)
        break

      case 'wordpress':
        getItems(targetId)
        break

      case 'landing':
        getItems(targetId)
        break
    }
  })
}
filter()

function getItems(className) {
  items.forEach((item) => {
    if (item.classList.contains(className)) {
      item.style.display = 'block'
    } else {
      item.style.display = 'none'
    }
  })
}

const $amazingWorks = $('.works-gallery-item')
console.log($amazingWorks)

$('.works-menu-item').click((event) => {
  const loadMoreBtns = document.querySelector('.loadMore')
  if (data - id === 'all') {
    loadMoreBtns.style.display = 'inline-block'

    const firstImages = $('.works-menu-item').slice(0, 4)
    for (let i = 0; i < firstImages.length; i++) {
      const Images = firstImages[i]
      console.log(Images)
      Images.classList.remove('tab-hidden')
    }
  } else {
    loadMoreBtns.style.display = 'none'
  }
})
