// 1
const getParagraph = document.getElementsByTagName('p')
console.log(getParagraph)
for (let i = 0; i < getParagraph.length; i++) {
  getParagraph[i].style.backgroundColor = '#ff0000'
}

// 2
const elemById = document.getElementById('optionsList')
console.log(elemById)

const parentsElem = elemById.parentElement
console.log(parentsElem)

const nodesChild = elemById.childNodes
console.log(nodesChild)

// 3
const allListElements = document.querySelector('#testParagraph')
allListElements.innerHTML = 'This is a paragraph'
console.log(allListElements)

//  4
const elemsByClass = document.querySelector('.main-header')
const elemsofHeader = elemsByClass.children
console.log(elemsofHeader)
// elemsofHeader.classList.replace('main-header', 'nav-item')
// console.log(elemsofHeader)

// 5
// Жодного єлементу з класом section-title не знайдено
const elemsDelete = document.getElementsByClassName('section-title')
for (let i = 0; i < elemsDelete.length; i++) console.log(elemsDelete)
