// Реалізувати функцію, яка отримуватиме масив елементів і виводити їх на сторінку у вигляді списку.
// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
// кожен із елементів масиву вивести на сторінку у вигляді пункту списку
// Приклади масивів, які можна виводити на екран:
// ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

const ul = document.createElement('ul')
let Arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
ul.innerHTML = Arr.map((item) => {
  return `<li>$(item)</li>`
}).join('')
console.log(ul.innerHTML)
document.body.prepend(ul)
setTimeout(() => ul.remove(), 3000)
