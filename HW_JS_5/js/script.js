// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
// Технічні вимоги:
// Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
// При виклику функція повинна запитати ім'я та прізвище.
// Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
// Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
// Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

function createNewUser() {
  const userName = prompt('Enter your name')
  const userLastName = prompt('Enter your last name')

  const newUser = {
    firstName: userName,
    lastName: userLastName,
    getLogin() {
      return this.firstName.toLowerCase() + this.userLastName.toLowerCase()
    },
  }
  return newUser
}
console.log(createNewUser)

const user = createNewUser()
console.log(user.getLogin())
